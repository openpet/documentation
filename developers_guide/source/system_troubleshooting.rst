.. this page is for debugging the system

.. role:: red

**********************
System Troubleshooting
**********************
    

Firmware
========
    
In order to troubleshoot the FPGAs on the support board and detector board, one can use a JTAG connection. For the support board, to test just the Main FPGA, use the JTAG along with the green jumper board as it is set up for normal operations (:numref:`sb_jtag`). 
    
.. _sb_jtag:

.. figure:: ./_static/OpenPET_Developers_Guide_Figures/sb_jtag.png
    :align: center
    :width: 40%
    
    Image of the JTAG connected to the support board.

To test the Main and IO FPGAs simultaneously, remove the green jumper board and use the JTAG chain depicted in :numref:`jtag_chain`. Each circle in the image represents a pin. The picture is rotated 90 degrees clockwise compared to how the pins are actually oriented on the board in the crate as can be seen when comparing :numref:`jtag_fpga_pins` to :numref:`jtag_chain`. Jumpers and shunts are used to connect the pins.

.. _jtag_fpga_pins:

.. figure:: ./_static/OpenPET_Developers_Guide_Figures/jtag_fpga_pins.png
    :align: center
    :width: 50%
    
    Picture of the pins used in the JTAG chain
    
.. _jtag_chain:

.. figure:: ./_static/OpenPET_Developers_Guide_Figures/JTAG_chain.png
    :align: center
    :width: 80%
    
    Diagram of the JTAG chain for debugging Main and IO FPGAs on the support board.
    
For the detector board, only the JTAG need be connected. This is shown in :numref:`db_jtag`.
    
.. _db_jtag:

.. figure:: ./_static/OpenPET_Developers_Guide_Figures/db_jtag.png
    :align: center
    :width: 50%
    
    Image of the JTAG connected to the detector board.

Embedded Software
=================    
    
With the JTAG connected, one can see what happens in NIOS for the support board and detector board. After connecting the JTAG to the desired board, open the NIOS command shell and then run the nios2-terminal executable. Now, when a command is sent to the system, the progress in NIOS can be seen. :numref:`nios2_cmd` shows an example of this output. 

.. _nios2_cmd:

.. figure:: ./_static/OpenPET_Developers_Guide_Figures/nios2_cmd.png
    :align: center
    :width: 80%
    
    Image of the NIOS command shell when a command is sent to the system.

Additionally, one can see what happens during the bootup sequence of the support board as shown in :numref:`nios2_bootup`.
    
.. _nios2_bootup:

.. figure:: ./_static/OpenPET_Developers_Guide_Figures/nios2_bootup.png
    :align: center
    :width: 80%
    
    Image of the NIOS command shell during the bootup sequence.
    
If one wants to see NIOS on both the support board and detector board simultaneously, connect a JTAG to each board and open two NIOS command shells. In one window, run the nios2-terminal command specifying which JTAG cable is to be used by adding a parameter. In the command :code:`nios2-terminal -c1`, the parameter c1 indicates the cable in slot 1 is to be used. To set up the second window, run :code:`nios2-terminal -c2` to view the information from the second board. 

To determine what cables are connected to the system, there is a command :code:`jtagconfig` that shows the list of cables. However, to determine which cable is connected to which board, one must physically look at the system setup. 

For more NIOSII command-line tools, see this `page <https://www.altera.com/content/dam/altera-www/global/en_US/pdfs/literature/hb/nios2/edh_ed51004.pdf>`_