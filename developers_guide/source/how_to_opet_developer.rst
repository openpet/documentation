.. this page is for the how to be opet developer section

.. role:: red

******************************
How To Be An OpenPET Developer
******************************

Signup as Developer
===================

In order to have read/write access to source code on the OpenPET BitBucket repository, users are required to sign up for an OpenPET 'developer' account. If you don't have a developer account, please send an email request to openpet@lbl.gov with the following information:

* Your name
* Institute/organization
* Names of individuals who will be working on the project
* What you intend to do with the source code
* Your potential contribution to this project

If your email does not include this complete information, then your request to be a Developer will not be granted. Once you have a developer account, you can view our BitBucket repository. 


BitBucket Repository
====================

  (Describe how to use BitBucket, how to get code approved, etc.)

Getting Started
---------------

    `BitBucket Getting Started <https://confluence.atlassian.com/bitbucket/>`_

  
.. _repo:

.. figure:: ./_static/OpenPET_Developers_Guide_Figures/Slide11.PNG
    :align: center
    
    OpenPET BitBucket repository



Contributer vs. Developer
-------------------------

There are two different levels of access that are possible and will be determined by the OpenPET administration. A contributor is given read and write access which means he can clone, edit, and push changes directly from a local machine. A developer is given read access only, meaning he can view or clone the repository. For a developer to merge his work with the default branch, he must submit a pull request. 


Code Style Requirements
=======================

:red:`(See Framework section 6.2.)`

C Programming (NOIS II)
=======================

:red:`(See Framework section 6.2.1. Include program file style, comments style, examples, etc.)`

VDHL Programming
----------------

:red:`(See Framework section 6.2.2?`

:red:`Look up CERN Open Hardware standard)`

:red:`VHDL CERN guide info at:`

	http://www.ohwr.org/documents/24
	http://www.ohwr.org/attachments/554/VHDLcoding.pdf
