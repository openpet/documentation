.. This page is for Abbreviations

Abbreviations
=============


| SB: Support Board
| DB: Detector Board
| MB: Multiplexer Board (also called high-speed data transceiver board)


| CI: Coincidence Interface Board
| CU: Coincidence Unit
| DU: Detector Unit
| CDU: Coincidence/Detector Unit


| CUC: Coincidence Unit Controller
| DUC: Detector Unit Controller
| CDUC: Coincidence/Detector Unit Controller


| FPGA: Field-programmable gate array
| NIOS II: A 32-bit embedded-processor architecture designed specifically for the Altera FPGAs
| VHDL: VHSIC hardware description language


| CRC: Cyclic redundancy check


| FSM: Finite State Machine
