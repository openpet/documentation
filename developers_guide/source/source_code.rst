.. this page is for the source code section

.. role:: red

***********
Source Code
***********


Introduction
============

.. container:: red

  (Describe source code modules: names, functions, how they tie in with previous higher level description shown in :numref:`firm_soft_struct`, etc.)


Firmware and Software
=====================


OpenPET Library
===============

This section describes the python OpenPET library (OpenPETlib.py) used in the openpet executable. The library file can be found in the hostpc repository under the software directory.

Variables
---------

* IMP_NETMAP - If netmap exists, set IMP_NETMAP to true. If not, set IMP_NETMAP to false.

.. tabularcolumns:: |p{6cm}|p{2.1cm}|p{7.5cm}|

.. table:: System Variables

  +--------------------------+-------------+-------------------------------------------------+
  |**Variable Name**         | **Value**   |                 **Description**                 |
  +==========================+=============+=================================================+
  |QUSB_FD_WIDTH             | 16          || Quick USB data bus width                       |
  +--------------------------+-------------+-------------------------------------------------+
  |QUSB_ADR_WIDTH            | 9           || Quick USB address bus width                    |
  +--------------------------+-------------+-------------------------------------------------+
  |QUSB_CMD_PKTS             | 5           || Number of Quick USB packets (write cycles)     |
  |                          |             || to complete a single OpenPET command.          |
  |                          |             || Current OpenPET command length is 80           |
  |                          |             || bits so 5x16=80                                |
  +--------------------------+-------------+-------------------------------------------------+
  |UDP_WIDTH                 | 8           ||                                                |
  +--------------------------+-------------+-------------------------------------------------+
  |UDP_CMD_PKTS              | 10          ||                                                |
  +--------------------------+-------------+-------------------------------------------------+
  |BITS_IN_BYTE              | 8           || Number of bits in one byte (for convenience)   |
  +--------------------------+-------------+-------------------------------------------------+
  |SRC_ADDR                  | 0x4000      || Workstation address such as the Host PC        |
  +--------------------------+-------------+-------------------------------------------------+
  |QUSB_CMD_LEN              |             || QUSB_FD_WIDTH * QUSB_CMD_PKTS / BITS_IN_BYTE   |
  |                          |             || Command length in bytes                        |
  +--------------------------+-------------+-------------------------------------------------+
  |UDP_CMD_LEN               |             || UDP_WIDTH * UDP_CMD_PKTS / BITS_IN_BYTE        |
  |                          |             || Command length in bytes                        |
  +--------------------------+-------------+-------------------------------------------------+
  |UDP_CMD_LEN_OS            |             || UDP_CMD_LEN unless UDP_CMD_LEN < 46. In which  |
  |                          |             || case, the value is 46 to prevent Windows error |
  +--------------------------+-------------+-------------------------------------------------+
  |UDP_DAT_LEN               | 1440        || If IMP_NETMAP is true, then it equals 8945     |
  +--------------------------+-------------+-------------------------------------------------+
  |PRINT_ACQ_PROG            | 200         || Number of clock ticks between printing system  |
  |                          |             || status                                         |
  +--------------------------+-------------+-------------------------------------------------+
  
  
.. tabularcolumns:: |p{6cm}|p{2.1cm}|p{7.5cm}|  
 
.. table:: Command responses
  
  +--------------------------+-------------+-------------------------------------------------+
  |**Variable Name**         | **Value**   |                 **Description**                 |
  +==========================+=============+=================================================+
  |CMD_STDCMD_UNKNOWN        | 0x7F00      || Software (running on NIOS) command id          |
  |                          |             || is unknown to node.                            |
  +--------------------------+-------------+-------------------------------------------------+
  |CMD_STDCMD_TIMEDOUT       | 0x7F01      || Software (running on NIOS) command id          |
  |                          |             || has timed out.                                 |
  +--------------------------+-------------+-------------------------------------------------+
  |CMD_STDCMD_DEADCHILD      | 0x7F02      || Targeted child is dead, nonexistent,           |
  |                          |             || or not programmed.                             |
  +--------------------------+-------------+-------------------------------------------------+
  |CMD_STDCMD_BUSYCHILD      | 0x7F03      || Targeted child is busy processing              |
  |                          |             || previous command.                              |
  +--------------------------+-------------+-------------------------------------------------+
  |CMD_FWCMD_UNKNOWN         | 0x7F04      || Firmware (running on FPGA fabric)              |
  |                          |             || command id is unknown to node.                 |
  +--------------------------+-------------+-------------------------------------------------+
  |CMD_FWCMD_TIMEDOUT        | 0x7F05      || Firmware (running on FPGA fabric)              |
  |                          |             || command id has timed out.                      |
  +--------------------------+-------------+-------------------------------------------------+
  |CMD_STDCMD_INCOMP         | 0x7F06      || Software (running on NIOS) received            |
  |                          |             || incomplete OpenPET command                     |
  +--------------------------+-------------+-------------------------------------------------+
  |CMD_STDCMD_FAST_USER      | 0x7F07      || Software (running on NIOS) received            |
  |                          |             || packets faster than it can handle.             |
  +--------------------------+-------------+-------------------------------------------------+
  |CMD_STDCMD_RESPONSE_MSK   | 0x8000      || Command response mask                          |
  +--------------------------+-------------+-------------------------------------------------+

  
.. table:: Data IDs

  +------------------+-------------+-------------------------------------------------+
  |**Variable Name** | **Value**   |                 **Description**                 |
  +==================+=============+=================================================+
  |DB_HDR_ID         | 0x40000000  || Packet ID value in the DB header to verify that|
  |                  |             || it is the DB header.                           |
  +------------------+-------------+-------------------------------------------------+
  |CH_HDR_ID         | 0x30000000  || Packet ID value in the channel header to verify|
  |                  |             || that it is the channel header.                 |
  +------------------+-------------+-------------------------------------------------+
  |DT_PKT_ID         | 0x10000000  || Packet ID value in the data packet to verify   |
  |                  |             || that it is a data packet.                      |
  +------------------+-------------+-------------------------------------------------+
  |PKT_ID_MSK        | 0xF0000000  || Packet ID mask                                 |
  +------------------+-------------+-------------------------------------------------+
  |DHEADERLEN        | 1000        || Data header length (words)                     |
  +------------------+-------------+-------------------------------------------------+
 
 
.. table:: QuickUSB variables

  +------------------+-------------+-------------------------------------------------+
  |**Variable Name** | **Value**   |                 **Description**                 |
  +==================+=============+=================================================+
  |QUSB_major        | 2           || QuickUSB driver major version number           |
  +------------------+-------------+-------------------------------------------------+
  |QUSB_minor        | 15          || QuickUSB driver minor version number           |
  +------------------+-------------+-------------------------------------------------+
  |QUSB_rev          | 2           || QuickUSB driver revision number                |
  +------------------+-------------+-------------------------------------------------+
  |QUSB_TIMEOUT      | 50          || QuickUSB timout in milliseconds                |
  +------------------+-------------+-------------------------------------------------+
  |DATABUFS          | 8           || QuickUSB default buffer size                   |
  +------------------+-------------+-------------------------------------------------+
  |DATABUFSIZE       | 2x512x1024  || Maximum buffer size                            |
  +------------------+-------------+-------------------------------------------------+

There is a final QuickUSB variable that sets the QuickUSB settings. It is a 2D array that is broken down below.

**QUSBSETTINGS**: 

  * [1, 0x0001] - Sets address 1 to 00000001.
  
    * Bit 7-1: Reserved
    * Bit 0: Sets data word width to 16 bits.
    
  * [2, 0xC000] - Sets address 2 to 1100000000000000.

    * Bit 15: Disables incrementing the address bus.
    * Bit 14: Disables address bus
    * Bit 13-9: Unused
    * Bit 8-0: HSPP address value
      
  * [3, 0x0002] - Sets address 3 to 0000000000000010. 
  
    * Bit 15-14: Unused R/O
    * Bit 13: Sets FIFO packet end polarity to active low
    * Bit 12: Sets FIFO output enable polarity to active low
    * Bit 11: Sets FIFO read polarity to active low
    * Bit 10: Sets FIFO write polarity to active low
    * Bit 9: Sets FIFO empty flag polarity to active low
    * Bit 8: Sets FIFO full flag polarity to active low
    * Bit 7: Sets IFCLK source to external clock
    * Bit 6: Sets IFCLK speed to 30MHz
    * Bit 5: Tri-state the IFCLK pin
    * Bit 4: Sets IFCLK polarity to normal
    * Bit 3: Sets GPIF clock mode to synchronous GPIF
    * Bit 2: Reserved
    * Bit 1-0: Sets HSPP configuration to GPIF master mode
  
  * [5, 0x8010] - Sets address 5 to 1000000000010000. 
  
    * Bit 15: Sets USB bus speed to allow high-speed (480Mbps)
    * Bit 14-8: Reserved
    * Bit 7-6: Unused R/O
    * Bit 5: Reserved
    * Bit 4-3: Sets CPU clock speed to 48MHz
    * Bit 2: Does not invert CLKOUT
    * Bit 1: Tri-state the CLKOUT pin
  
  
.. table:: SRAM commands

  +--------------------------+-------------+-------------------------------------------------+
  |**Variable Name**         | **Value**   |                 **Description**                 |
  +==========================+=============+=================================================+
  |CMD_STDCMD_SRAM_WRITE     | 0x000B      || Writes to external SRAM device.                |
  |                          |             || Auto-increments address.                       |
  +--------------------------+-------------+-------------------------------------------------+
  |CMD_STDCMD_SRAM_READ      | 0x000C      || Reads from external SRAM device.               |
  |                          |             || Auto-increments address.                       |
  +--------------------------+-------------+-------------------------------------------------+

  
.. table:: UDP Variables

  +--------------------------+-------------+-------------------------------------------------+
  |**Variable Name**         | **Value**   |                 **Description**                 |
  +==========================+=============+=================================================+
  |UDP_CMD_PORT              | 9955        ||                                                |
  |                          |             ||                                                |
  +--------------------------+-------------+-------------------------------------------------+
  |UDP_DAT_PORT              | 9956        ||                                                |
  |                          |             ||                                                |
  +--------------------------+-------------+-------------------------------------------------+
  |UDP_DAT_PORT_HEX          |             || struct.pack(">H", UDP_DAT_PORT)                |
  |                          |             ||                                                |
  +--------------------------+-------------+-------------------------------------------------+
  
.. table:: UDP Commands
 
  +--------------------------+-------------+-------------------------------------------------+ 
  |**Command**               | **Value**   |                 **Description**                 |
  +==========================+=============+=================================================+
  |c_UDP_CMD_ACK             | 0xAC        || Request                                        |
  |                          |             ||                                                |
  +--------------------------+-------------+-------------------------------------------------+
  |c_UDP_CMD_LEN             | 0xBD        || Payload length                                 |
  |                          |             ||                                                |
  +--------------------------+-------------+-------------------------------------------------+
  |c_UDP_CMD_MODE_ACQ        | 0xC1        || Default mode                                   |
  |                          |             ||                                                |
  +--------------------------+-------------+-------------------------------------------------+
  |c_UDP_CMD_MODE_TST        | 0xC2        || Test mode                                      |
  |                          |             ||                                                |
  +--------------------------+-------------+-------------------------------------------------+
  |c_UDP_CMD_MODE_NOACK      | 0xC3        || Openloop test mode                             |
  |                          |             ||                                                |
  +--------------------------+-------------+-------------------------------------------------+
  |c_UDP_CMD_MODE_IDL        | 0xC4        || Openloop idle mode                             |
  +--------------------------+-------------+-------------------------------------------------+
  
.. note:: UDP commands are NOT 80-bit OpenPET commands
  
  
.. table:: Network friendly bytes

  +--------------------------+-------------------------------------------------+
  |**Variable Name**         |                   **Description**               |
  +==========================+=================================================+
  |UDP_B_REQ                 || struct.pack('>B',c_UDP_CMD_ACK)                |
  |                          || Construct byte for Request                     |
  +--------------------------+-------------------------------------------------+
  |UDP_B_ACQ                 || struct.pack('>B',c_UDP_CMD_MODE_ACQ)           |
  |                          || Construct byte for default mode                |
  +--------------------------+-------------------------------------------------+
  |UDP_B_TST                 || struct.pack('>B',c_UDP_CMD_MODE_TST)           |
  |                          || Construct byte for test mode                   |
  +--------------------------+-------------------------------------------------+
  |UDP_B_LEN                 || struct.pack('>BH',c_UDP_CMD_LEN, UDP_DAT_LEN)  |
  |                          || Construct byte payload length                  |
  +--------------------------+-------------------------------------------------+
  |UDP_B_NOACK               || struct.pack('>B',c_UDP_CMD_MODE_NOACK)         |
  |                          || Construct byte for openloop test mode          |
  +--------------------------+-------------------------------------------------+
  |UDP_B_IDL                 || struct.pack('>B',c_UDP_CMD_MODE_IDL)           |
  |                          || Construct byte for openloop idle mode          |
  +--------------------------+-------------------------------------------------+
  
  
  
Functions
---------
  
* :code:`__enter__(self)`:

    Class initialization function for proper QuickUSB handling.

* :code:`__exit__(self, exc_type, exc_value, traceback)`:

    Class clean up function for proper QuickUSB handling.

* :code:`__init__(self,qusbindx=0)`:

    Class constructor. Sets values for various settings of things such as QuickUSB, scope mode acquisition, DAC, etc.
    
    The following table explains the variables in the OpenPET class constructor.
    
    ============================== ===============
    Variable                       Description
    ============================== ===============
    qusb                           Contains the QuickUSB object
    qusb_valid                     Used to check that QuickUSB is valid
    ofilename                      Default file name
    ofile                          Data output file
    cmd_retries                    Number of times to retry an OpenPET command 
    cmd_timeout                    Length of time in seconds until timeout
    d_acq_t                        Length of time in seconds to acquire data (default = 10)
    d_acq_osc_trig_win             Scope mode: trigger window (default = 5)
    d_acq_osc_samples_before_trig  Scope mode: number of samples before trigger (default = 6)
    d_acq_osc_samples              Scope mode: number of ADC samples (default = 32)
    d_acq_osc_format               Scope mode: data format (default = 1)
    d_dac_vpp                      DAC resolution (default = 2^10)
    d_dac_v                        DAC threshold (default = 200)
    d_dac_type                     DAC type (timing = 0, energy = 1)
    d_process_data                 Flag to determine if data will be processed (default = 1)
    d_qu                           Output queue
    qusb_streamid                  QuickUSB streaming ID
    tbytes                         Number of bytes QuickUSB collects
    wbytes                         Number of bytes written to disk
    d_mode                         OpenPET acquisition mode (scope, singles, etc)
    d_mode_stgs                    OpenPET acquisition mode settings
    timelapse                      Start time of acquisition
    data                           OpenPET data
    outofmem                       Flag to indicate if ran out of memory
    eth                            Contains Ethernet object
    ip_src                         IP source address
    mac_src                        MAC source address
    mac_dst                        MAC destination address
    ip_dst                         IP destination address
    ethname                        Name of Ethernet device
    ip_brdst                       Broadcasting IP
    d_netmap                       Ethernet netmap
    eth_thru_test                  Flag to enable/disable Ethernet throughput test
    raw_pkt_id                     Unique ID for tx packets
    frame_loss                     Frame loss detection
    pktcnt                         Counter for frames received
    ============================== ===============

* :code:`list_eth(self)`:

    Gets the list of available network interfaces. If there are none, an error message will appear.
    
    * **Returns**: List of network interfaces
    
* :code:`list_qusb(self)`:

    Gets the list of available QuickUSB devices. If there are no QuickUSB devices, an error message will appear.
    
    * **Returns**: List of QuickUSB devices

* :code:`init_qusb(self, devindex=0)`:

    Initializes, checks, and configures QuickUSB. It checks that the correct QuickUSB model, DLL version, driver version, and firmware version are installed. If there is an error, a message will appear with information on what needs to be fixed.
    
    =========== ===================================================
    **Keyword Arguments**
    ---------------------------------------------------------------
    devindex    QuickUSB device index
    =========== ===================================================

    * **Returns**: QuickUSB object
    
* :code:`init_eth(self, eindex, ip_dst)`:

    Initializes, checks, and configures Ethernet interface. It checks that the user supplied an Ethernet index. It also checks if the user supplied a destination IP address. If no IP address is given, then broadcast is used. If there is an error, a message will appear with information on what needs to be fixed.
    
    =========== ===================================================
    **Keyword Arguments**
    ---------------------------------------------------------------
    eindex      Ethernet index
    ip_dst      destination IP address
    =========== ===================================================

    * **Returns**: True or False

* :code:`rcmd(self)`:

    Reads OpenPET command or response
    
    * **Returns**: Command read

* :code:`wcmd(self, command)`:

    Writes OpenPET command
    
    =========== ===================================================
    **Keyword Arguments**
    ---------------------------------------------------------------
    command     OpenPET command to be written
    =========== ===================================================
    
    * **Returns**: QuickUsb.WriteCommand() return value

* :code:`openpet_cmd(self, cmd_id, cmd_dst, cmd_payload)`:

    Executes OpenPET Hardware/Firmware/Software commands. If there is an error, a message will appear explaining what the error is. If QuickUSB is being used, openpet_cmd_qusb command is executed, and if Ethernet is being used, openpet_cmd_eth command is executed.
    
    =========== ===================================================
    **Keyword Arguments**
    ---------------------------------------------------------------
    cmd_id      command ID
    cmd_dst     destination address
    cmd_payload command payload
    =========== ===================================================

    * **Returns**: Command ID, source, destination, and command payload (if available)   

* :code:`openpet_cmd_eth(self, cmd_id, cmd_dst, cmd_payload)`:
    
    Sends OpenPET command through Ethernet.
  
    =========== ===================================================
    **Keyword Arguments**
    ---------------------------------------------------------------
    cmd_id      command ID
    cmd_dst     destination address
    cmd_payload command payload
    =========== ===================================================

    * **Returns**: Command ID, source, destination, and command payload (if available)
    
* :code:`openpet_cmd_qusb(self, cmd_id, cmd_dst, cmd_payload)`:
    
    Sends OpenPET command through QuickUSB.
  
    =========== ===================================================
    **Keyword Arguments**
    ---------------------------------------------------------------
    cmd_id      command ID
    cmd_dst     destination address
    cmd_payload command payload
    =========== ===================================================

    * **Returns**: Command ID, source, destination, and command payload (if available)
 
* :code:`openpet_cmd_vresponse(self, cmd_id, response)`:

    Checks that the OpenPET response is valid. If there is an error, an error message is printed describing the error more specifically.
    
    =========== ===================================================
    **Keyword Arguments**
    ---------------------------------------------------------------
    cmd_id      command ID
    response    response received
    =========== ===================================================
    
    * **Returns**: True or False if valid or invalid respectively.
    
* :code:`opendatafile(self, mode)`:

    Creates and opens data output binary file
    
    * **Returns**: File object

* :code:`closedatafile(self)`:

    Closes data output binary file
    
    * **Returns**: File object close() value

* :code:`get_dac_bits(self, vol=None, dactype=None, vpp=None)`:

    Returns the correct bits for a given DAC voltage

    ======== ======================================================
    **Keyword Arguments**
    ---------------------------------------------------------------
    vol      voltage requested
    dactype  type of DAC, energy = 1 or timing = 0 (default)
    vpp      range of the DAC, e.g., 2^10 for 10-bit DAC
    ======== ======================================================

    * **Returns**: 10-bit DAC data payload

* :code:`resolve_osc_stg_payload(self, payload)`:

    Gets the scope mode settings from the payload. If no argument is provided, class values are returned
    
    ======== ======================================================
    **Keyword Arguments**
    ---------------------------------------------------------------
    payload  32-bit scope mode settings payload
    ======== ======================================================    

    * **Returns**: 
      
      * d_acq_osc_trig_win - trigger window
      * d_acq_osc_samples_before_trig - number of ADC samples before energy trigger
      * d_acq_osc_samples - total number of ADC samples
      * d_acq_osc_format - used internally for data handling 
    

* | :code:`set_osc_stg_payload(self, d_acq_osc_trig_win, d_acq_osc_samples_before_trig, d_acq_osc_samples,` 
  | :code:`d_acq_osc_format)`:
  
    Sets the scope mode settings with given function arguments and creates a corresponding payload. If no arguments are provided, class values are returned.
    
    ============================= =================================
    **Keyword Arguments**
    ---------------------------------------------------------------
    d_acq_osc_trig_win            trigger window
    d_acq_osc_samples_before_trig number of ADC samples before energy trigger
    d_acq_osc_samples             total number of ADC samples
    d_acq_osc_format              used internally for data handling (must be 1)
    ============================= =================================

    * **Returns**: 32-bit scope mode settings payload

* :code:`getdsettings(self)`:

    Gets acquisition mode settings from CDUC
    
    * **Returns**: Acquisition mode settings payload

* :code:`setactionrun(self, ignore=0)`:

    Sets the CDUC's Acquisition Action to Run and broadcasts to all nodes.
    
    =========== ===================================================
    **Keyword Arguments**
    ---------------------------------------------------------------
    ignore      if 1, skip the return value of OpenPET
    =========== ===================================================

* :code:`setactionreset(self)`:

    Sets the CDUC's Acquisition Action to Reset and broadcasts to all nodes.

* :code:`writeacqheader(self)`:

    Writes Acquisition Header to binary data file.

* :code:`getdata(self, runreset=1)`:

    Start and acquire data through QuickUSB or Ethernet.
    
    =========== ===================================================
    **Keyword Arguments**
    ---------------------------------------------------------------
    runreset    if 1, use default behavior      
    =========== ===================================================

* :code:`stopdata_qusb(self)`:

    Stop data acquisition from QuickUSB. 
    
* :code:`stopdata_eth(self)`:

    Stop data acquisition from Ethernet 

* :code:`ctype2uint32(self, data, nbytes)`:

    Converts ctype data to uint32 vector

    ========== ====================================================
    **Keyword Arguments**
    ---------------------------------------------------------------
    data       Pointer to memory buffer
    nbytes     Length of memory buffer
    ========== ====================================================

    * **Returns**: uint32 data vector    
    

* :code:`data_cb(self, bstreamobj)`:

    Callback function for QuickUSB streaming API. This function also removes unnecessary zeros from the data.

    ========== ====================================================
    **Keyword Arguments**
    ---------------------------------------------------------------
    bstreamobj a pointer of type PQBULKSTREAM to QBULKSTREAM object
    ========== ====================================================      
    

* :code:`qu_writeraw(self, q)`:

    Queue function to write raw data to file. For Ethernet connections, this function removes unnecessary zeros in the data. This function is a wrapper function that calls qu_write in the end.

    ========= =====================================================
    **Keyword Arguments**
    ---------------------------------------------------------------
    q         reference to Queue()
    ========= =====================================================

* :code:`qu_write(self, data)`:

    Writes raw data to file.

    ========= =====================================================
    **Keyword Arguments**
    ---------------------------------------------------------------
    data      data to write
    ========= =====================================================


* :code:`loaddatafiletomemory(self, filename)`:

    Load file content to memory

    ======== ======================================================
    **Keyword Arguments**
    ---------------------------------------------------------------
    filename file object
    ======== ======================================================

    * **Returns**: uint32 numpy array of file content    

* :code:`writesramtofile(self, filename,dst_addr, size=1024*1024*2, offset=0)`:

    Write SRAM content from OpenPET to disk
    
    ======== ======================================================
    **Keyword Arguments**
    ---------------------------------------------------------------
    filename file object
    dst_addr OpenPET destination address (don't use broadcast here)
    size     number of bytes to write to file
    offset   offset address in SRAM to read from
    ======== ======================================================


* :code:`writefiletosram(self, filename, dst_addr, offset=0)`:

    Write SRAM content from disk to OpenPET
    
    ======== ======================================================
    **Keyword Arguments**
    ---------------------------------------------------------------
    filename file object
    dst_addr OpenPET destination address (don't use broadcast here)
    offset   offset address in SRAM to write to
    ======== ======================================================

    
  .. note:: Writes 32-bits at a time

* :code:`str2num(self, x)`:

    Converts a string number from a decimal or hex to an integer. 
    
    ======== ======================================================
    **Keyword Arguments**
    ---------------------------------------------------------------
    x        string number
    ======== ======================================================

    * **Returns**: Integer

* :code:`show_acq_progress(self)`:

    Prints acquisition progress.

* :code:`ipv4innetwork(self, ip_src, ip_dst, netmask)`:

    Checks is the IP address is within a network mask.
    
    ======== ======================================================
    **Keyword Arguments**
    ---------------------------------------------------------------
    ip_src   IP address of local interface
    ip_dst   IP address of OpenPET chassis
    netmask  netmask of local interface
    ======== ======================================================

    * **Returns**: True or False   

* :code:`build_nm_packet(self, byteit)`:

    Builds a raw UDP packet to be used in netmap.
    
    ======== ======================================================
    **Keyword Arguments**
    ---------------------------------------------------------------
    byteit   byte to send
    ======== ======================================================
    
    * **Returns**: UDP packet
    
* :code:`send_raw_packet_nm(self, byteit)`:

    Sends raw packet using netmap tx ring.
    
    ======== ======================================================
    **Keyword Arguments**
    ---------------------------------------------------------------
    byteit   byte to send
    ======== ======================================================
    
* :code:`ip_chksum(self, phead)`:

    Calculates IP header checksum.
    
    ======== ======================================================
    **Keyword Arguments**
    ---------------------------------------------------------------
    phead    IP header packet   
    ======== ======================================================
    
    * **Returns**: 16-bit checksum

OpenPET Control and Analysis Tools
==================================

OpenPET Control and Analysis Tools (OpenPET CAT) is a data acquisition and analysis software for the OpenPET electronics based on the ROOT framework. OpenPET CAT utilizes the object-oriented design in providing basic utilities, control and analysis tools for the OpenPET electronics. Using OpenPET CAT requires installing the `ROOT package <http://root.cern.ch>`_. 

Using OpenPET CAT is optional; users can run the executables "openpet" (see :ref:`System Commands and Responses` section) to configure the system and to acquire data directly.

Installing ROOT
---------------

Go to `Downloading ROOT <http://root.cern.ch/drupal/content/downloading-root>`_ for instructions on downloading and installing ROOT. Although ROOT is supported on many platforms, OpenPET only supports the Windows version of ROOT using the Microsoft Visual C++ compiler. Current OpenPET CAT supports ROOT version 5.34/30 and Microsoft Visual C++ 2010. If you plan to develop and compile OpenPET CAT codes, you also need to install Microsoft Visual C++ 2010 Express, which can be downloaded at no cost from `Visual Studio Downloads <http://www.visualstudio.com/downloads/download-visual-studio-vs>`_, and `Cygwin <https://www.cygwin.com/>`_.

For instructions on how to use OpenPET CAT, see the `OpenPET CAT section <http://openpet.readthedocs.org/en/latest/openpet_cat.html>`_ in the User's Guide.

OpenPET CAT Repository
----------------------

The OpenPET CAT repository consists of six folders.

1. src - Contains all source files
2. inc - Contains all header files
3. bin - Once the code is complied, contains all executables
4. lib - Once the code is compiled, contains the OpenPET CAT library
5. doc - Contains documentation
6. examples - Contains example macros and configuration files

Installing OpenPET CAT
----------------------

First, clone the OpenPET HostPC repository and follow the instructions in the README file found in the sw directory. Next, clone the OpenPET CAT repository. After cloning, open cygwin and navigate to the src directory. Run the following commands to build OpenPET CAT.

::
  
  $ make clean
  $ make
  
After it is built, a GUI executable openpet_cat.exe will appear in the bin directory, and the OpenPET CAT library file libopenpetcat.dll will appear in the lib directory. If ROOT was previously installed, this library will also be copied to the directory <installed ROOT directory>/bin (e.g., $ROOTSYS/bin). If ROOT has not been installed, install ROOT as outlined above and then copy the library file into the ROOT bin directory.

The OpenPET CAT software is now ready to use. For further instructions, please refer to the `OpenPET CAT section <http://openpet.readthedocs.org/en/latest/openpet_cat.html>`_ in the User's Guide.

:red:`(If optional tool, then this should be in a separate document?)`