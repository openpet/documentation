.. this page is for the what exists now section

.. role:: red

***************
What Exists Now
***************

.. container:: red

	(Previous sections describe the overall, long-term development plans. Describe here what has already been implemented in version 1.0 Oscilloscope mode.)

	Use what subsections?
