# README #

OpenPET Documentation is a repository of the documentation for the OpenPET system.  The files are written in reStructuredText and can be generated locally using Sphinx and LaTex into html and pdf pages respectively.  Currently, there are two documents: Developer's Guide and User's Guide.  Each of these have their own folder in this repository.  The documentation is hosted by Read the Docs.

### OpenPET Documentation Repository ###

This repository consists of three folders.

  1. developers_guide - Developer's Guide directory
  2. users_guide - User's Guide directory
  
The developers_guide and users_guide directories have the following files and folders:

  1. source directory - contains all the required rst files and the following two directories

    a) _static directory - contains all images, css files, and OPET figures powerpoint

    b) _templates directory - contains outside template files

  2. Makefile
  3. make.bat

The documentation directory  contains the following files and folders:

  1. docs directory
  2. site directory
  3. mkdocs.yml

### Build Locally ###

To build locally on Windows, one must have Python, Sphinx, and LaTex (or equivalent) installed.

Installing Python - If possible, install the latest Python 2 version.  These instructions are for downloading Python 2.7.10.  The advantage of installing Python 2.7.10 or later is that pip comes with it.  The Windows installer can be found here: https://www.python.org/downloads/release/python-2710/  Select the correct Windows MSI installer (bottom two) for your computer.  Follow these instructions to install only Python: http://sphinx-doc.org/install.html#windows-install-python-and-sphinx  Also change the environment variables as outlined in the instructions for convenience.
  
Installing Sphinx - After installing Python and changing the environment variables, follows these instructions to install Sphinx: https://docs.readthedocs.org/en/latest/getting_started.html  This document also outlines how to start a new project even though that is not necessary to contribute to pre-established documentation in this repository. It might also be necessary to install the sphinx_rtd_theme. To do so, run the following command: pip install sphinx_rtd_theme

Installing LaTex - Sphinx gives the option to generate the required tex documents so that one can generate a pdf file using some sort of Tex environment.  These steps outline installing MikTex to create the pdf documents.  MikTex can be downloaded here: http://miktex.org/2.9/setup  After downloading, run the installer and change the PATH variable as outlined here: http://stackoverflow.com/questions/13155509/how-to-create-pdf-documentation-with-sphinx-in-windows

### Contribution guidelines ###

When pushing modified documents or adding documents, only add files from the source directory (i.e., not the build directory)

On adding figures: 

 - Double check image file extension case. Images generated from the powerpoint file have upper case extensions (PNG) while other images (such as those downloaded from LucidChart) are lower case (png).  TIFF is not supported.
 - Cropping images is not yet supported in reStructuredText.  Therefore, image modification must be done beforehand.