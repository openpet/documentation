.. OpenPET User's Guide documentation master file, created by
   sphinx-quickstart on Thu Jul 16 11:58:35 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

OpenPET User's Guide
====================


.. toctree::
   :maxdepth: 2

	 Abbreviations <abbrev>
	 Release Notes <release_notes>
	 System Overview <system_overview>
	 Getting Started <getting_started>
	 Detector Board <detector_board>
	 Support Board <support_board>
	 Commands <commands>
	 Control and Analysis Tools <openpet_cat>
	 Acknowledgements <acknowledgements>
	 Appendices <appendices>
	 Errata <errata>

   
Index
=====   
   
* :ref:`genindex`

.. * :ref:`modindex`

.. * :ref:`search`

