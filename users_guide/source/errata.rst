.. This page is the Errata section

.. role:: red

.. role:: crop

.. role:: bash(code)
   :language: bash

.. index:: Errata

.. _errata:

***************************
Errata
***************************
For an up-to-date list see the *issues* page in Bitbucket repository: `SupportBoard <https://bitbucket.org/openpet/supportboard/issues?status=new&status=open>`_, `DetectorBoard <https://bitbucket.org/openpet/detectorboard16chlbnl/issues?status=new&status=open>`_,  and `Software <https://bitbucket.org/openpet/hostpc/issues?status=new&status=open>`_.


Firmware
========
* Singles example supports 1 DB.
* Singles example computes the energy for the first four channels on a given DB.
* Singles example uses the external energy trigger. Firmware trigger is not available.
* Standard System Scope mode supports short acquisitions only. Data corruption will occur for long acquisitions.

SupportBoard
------------



DetectorBoard
-------------
* High Performance TDC is disabled by default. Rebuild firmware with ``g_en_tdc : boolean  := true;`` to enable it.


Embedded Software
==================

SupportBoard
------------
Main FPGA
^^^^^^^^^^

IO FPGAs
^^^^^^^^^

DetectorBoard
-------------


Software
========

Hardware
========
* DetectorBoard front panel LEDs: CLK OK is swapped with V OK.