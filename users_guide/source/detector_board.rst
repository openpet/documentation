.. This page is for the Detector Board section

.. role:: red

.. index:: Detector Board

.. _`Detector Board`:

**************
Detector Board
**************

The purpose of the Detector Board is to accept analog inputs from the detector modules and convert them into singles event words. Generally, this requires determining the energy, interaction position, and arrival time associated with a single gamma ray interaction that occurs in the detector module, as well as applying as many corrections as possible before the associated singles event word is generated. This board must also have the ability to produce "singles events" that have alternate event formats, which are necessary for debugging, calibration, etc.

In order to process an analog input signal, the Detector Board contains an analog front-end circuit with amplification and filtering to reduce the noise and bandwidth of the signal. The processed analog signal is subsequently digitized by an analog-to-digital converter (ADC) to capture the processed analog signal. For systems that require good timing resolution such as PET, additional circuitry may be implemented in the front end to split the analog input signal into a fast timing path where the signal is amplified with a high-bandwidth amplifier. The amplified signal is then triggered by a fast leading edge discriminator to create a timing pulse for the signal arrival, which is time stamped by a time-to-digital converter (TDC) implemented inside the FPGA. 

The back end of the Detector Board primarily consists of an FPGA and static random access memory (SRAM). The FPGA processes the digitized signals from the ADCs and, if necessary, combines information from multiple channels to compute the deposited energy, the interaction position, and the event time. Appropriate calibration correction factors that are stored in the SRAM can also be applied to the data. The computed information are formatted into a singles event word and then transferred to the Support Board (i.e., SB-DUC) via the standard OpenPET Bus IO, as described in the `Bus IO`_. In addition, the firmware for the FPGA is loaded into the Detector Board by the Support Board via the standard Bus IO. Different firmware can be loaded into the DB FPGA to perform tasks other than event processing, such as debugging, testing, and calibration.

While the details of the signal processing depend strongly on the details of the detector module, the following is an example describing the processing performed for a block detector module with four analog outputs. Event processing is initiated by the OR of the low to high transitions from each of the timing signals. The total amount of signal observed by each of the four PMTs (A, B, C, & D) is computed by summing the output of each ADC for an appropriate period of time (typically 2-3 times the decay time of the scintillator). If necessary, pulse pile-up correction is also applied. These four signals are then summed to get a raw estimate of the energy (E=A+B+C+D), and the appropriate Anger logic estimators are computed (X=(A+B)/E, Y=(A+C)/E). Note that a fast division algorithm can be implemented using look-up tables in the detector memory. The X and Y values are used to address a crystal map table that resides in the detector memory, and so assign a crystal of interaction via a look-up table. The raw energy E and the crystal of interaction are again used to address another look-up table in the detector memory, and so determine whether the event satisfies the energy window criteria. Each timing signal is input to a TDC that is implemented in the detector FPGA, and so measures the (raw) position of the arrival time of each signal within a time slice, and a timing estimator computed from these digitized arrival times. The crystal of interaction and raw arrival time are used to address a look-up table stored in the detector memory and create a corrected arrival time. Thus, the position (crystal of interaction), energy, and arrival time are all computed. If the event satisfies the energy window criteria, these data are formatted to create a singles event word, and then passed to Support Board through the Bus IO block.

Several versions of the DB will be designed, mostly differing in how the analog inputs are being processed by the front-end circuitries and in the number of analog input channels per DB. The design details are described below.

.. index:: Bus IO

.. _busio:

Bus IO
======

The Bus IO connecting the Detector Board to the Support Board (SB-DUC) is shown schematically in :numref:`bus_io`. The bus is divided into several blocks.

One block provides the timing signals using 4 LVDS differential pairs. There are two fundamental timing signals - an 80 MHz system clock signal generated on the Support Board and a time slice boundary signal that defines the beginning of a time slice. The Support Board sends a copy of these system clock and time slice boundary signals, which are used to clock data from the Support Board to the Detector Board. Because there is propagation delay within the Detector Board, another copy of the system clock and time slice boundary signals (that is produced by the DB) is used to clock data from the Detector Board to the Support Board. In the OpenPET architecture, the system divides time into small, fixed time slices of either 100 ns (8 clocks) or 200 ns (16 clocks). Alternatively, custom time slices can be designed by modification of the firmware, if needed. 

A second block defines 4 LVTTL single-ended lines to control data between the DB and SB using a standard digital serial protocol (e.g., SPI).

A third block in the Bus IO provides 4 LVTTL single-ended lines to program the FPGA on the Detector Board using serial protocols, which are generated by the Support Board. 

A fourth block provides 16 differential pairs (grouped into 4 sets of 4 LVDS differential pairs) to transfer singles event words from the DB to SB. All individual operations must occur within a single time slice, which implies that only singles event words that occur in the same time slice can be combined to form a coincident event. A singles event word can be either a 32-bit word (100 ns time slice) or 64-bit word (200 ns time slice). Since it can take significantly longer than a single time slice to fully process a singles event, the system is pipelined so that the processing is divided into smaller steps that each can be completed in a single time slice. During one time slice, each set of 4 differential pairs can pass one singles event word (i.e. maximum of 4 singles event words per time slice). Thus, the maximum singles event rate that can be transferred out of each Detector Board is 40 million events per second. 

Another block provides 8 spare LVDS differential pairs between the DB and SB for users to pass any information between these two boards.

The final block in the BUS IO supplies power to the Detector Board. Specifically, the Support Board supplies +5 V, +3.3 V, -5 V, and ground.

:numref:`connections` details the connections between the Support Board and the Detector Board.

.. _bus_io:

.. figure:: ./_static/OpenPET_Users_Guide_Figures/Slide3.PNG
		:align: center
		:scale: 70%
		
		Diagram of the BUS IO.
		
		
.. _connections:

.. figure:: ./_static/OpenPET_Users_Guide_Figures/Slide6.PNG
		:align: center
		:scale: 70%
		
		Connections between the Support Board and the Detector Board.
		

.. index:: 16-Channel Detector Board

.. _`16-Channel DB`:
		
16-Channel Detector Board
=========================

.. _16ChDB:

.. figure:: ./_static/OpenPET_Users_Guide_Figures/Slide8.PNG
		:align: center
		:scale: 70%
		
		Block diagram of the 16-channel Detector Board. 

:red:`(Make new version. Replace clock, power, etc lines from the FPGA to 96-pin as BUS IO  -- i.e. shown in Fig 32? )`

A schematic of the block diagram of the 16-channel Detector Board is shown in :numref:`16ChDB` and a photograph is shown in :numref:`DBphoto`. This DB accepts up to 16 analog input signals, each of which is processed independently. The analog input signal is required to be negative polarity, ground referenced. The input analog signal is terminated with 50 ohms, and then split into two processing chains: an energy chain and a timing chain.

The processing circuit for one channel is shown in :numref:`channel_circuit`. The input stage accepts voltages between -0.8 V to 0 V. The input voltage can be attenuated to fall within the acceptable range by changing the attenuation resistor values on the energy chain. Only the signal on the energy chain needs to be attenuated, because the range of the input voltage is limited by the dynamic range of the ADC. In addition, there are 8 external differential LVDS IO pairs that can be used to interface to the DB FPGA. The flexibility of these digital IO allow any digital inputs and/or outputs to be applied to the Detector Board.

The energy chain amplifies the input signal and then splits the amplified signal into a comparator and an anti-aliasing filter with a cut-off frequency of 7 MHz. The comparator provides a trigger signal to start event processing; the trigger threshold is controlled by the energy DAC. The filtered output is sent to an ADC with programmable digital gain that digitizes the analog signal with a sampling rate between 10 MSPS to 65 MSPS. 

The timing chain amplifies the input signal with a high-gain high bandwidth amplifier, followed by an ultra fast discriminator that converts the analog signal into a digital timing signal whose leading edge is synchronized to the interaction time. The ADC values and the timing signal are sent to the DB FPGA.

Inside the DB FPGA, a TDC generates a time stamp indicating the arrival time of the timing signal relative to the time slice boundary. The DB FPGA and its memory also analyzes the ADC data from this channel and (potentially) combines it with information from other channels to compute the energy deposit, the interaction position, and the event time. Appropriate calibration correction factors are also stored in the memory and applied to the data.

.. _DBphoto:

.. figure:: ./_static/OpenPET_Users_Guide_Figures/Slide29.PNG
		:align: center
		:scale: 70%
		
		Photograph of the 16-channel Detector Board.
		
		
.. _channel_circuit:

.. figure:: ./_static/OpenPET_Users_Guide_Figures/Slide9.PNG
		:align: center
		:scale: 70%
		
		Block diagram of the front-end circuitries of one channel of the 16-channel Detector Board.
		


Analog Signal Conditioning
^^^^^^^^^^^^^^^^^^^^^^^^^^

Each input signal is required to be negative polarity. The signal is terminated, and then split into two processing chains: timing chain and energy chain. The input stage accepts voltages between -0.8 V to 0 V and has input diodes to protect against over and under voltage. The range of the input voltages is limited in part by the dynamic range of the ADC. The input voltage can be attenuated to fall within the acceptable range by changing the attenuation resistor values where the signal is split into the timing chain and energy chain. Only the signal on the energy chain needs to be attenuated.

Timing Signal
^^^^^^^^^^^^^

This circuit is a high-speed leading edge discriminator with a threshold that is controlled via a timing DAC. The timing edge is a high to low transition.

ADC
^^^

Each analog input signal is digitized by a 12-bit ADC with digital programmable gain that digitizes the analog signal at a sampling rate ranging from 10 MSPS to 65 MSPS.

TDC
^^^

Each analog input signal is digitized by a 12-bit ADC with digital programmable gain that digitizes the analog signal at a sampling rate ranging from 10 MSPS to 65 MSPS.

Detector Memory
^^^^^^^^^^^^^^^

Two MBytes of SRAM memory is attached to and controlled by the DB FPGA. This control also includes loading the contents of the memory.


Analog Input Connections
^^^^^^^^^^^^^^^^^^^^^^^^

:numref:`16dbpin` and the discussion below detail the analog input connections to the 16-channel Detector Board. Note that these inputs can be from a user-supplied analog conditioning circuit board (e.g., a preamplifier board), so the connector contains pins used to provide power to and communicate with one of these (optional) circuit boards.

Analog Inputs:

There are 16 analog input channels on the 16-channel DB. Each is a single-ended, negative polarity input signal with a negative-going leading edge. Input voltage levels should be between -0.8 V and 0 V.

Power and Ground:

The 16-channel DB is capable of supplying power to an analog conditioning circuit board. It supplies +5V, -5V, and ground. It is assumed that this is "digital quality" power - that the analog conditioning board will use these as inputs to on-board regulators to create analog quality power, as well as whatever other digital voltages are necessary. The maximum current for each of the voltage supplies is 1 A.

Connector:

We use a 68-pin D-sub connector with 0.050" Pitch x 0.100 Row to Row. The part number is AMP-Part-5787169-7 (Digi-Key part # A33512-ND).

.. _16dbpin:

.. figure:: ./_static/OpenPET_Users_Guide_Figures/Slide7.PNG
		:align: center
		:scale: 70%
		
		Pin assignment for the Analog Input Connector to the 16-channel Detector Board.