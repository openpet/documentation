.. This page is for OpenPET CAT


.. index:: OpenPET CAT

.. _opet-cat:

************************************************
OpenPET Control and Analysis Tools (OpenPET CAT)
************************************************

OpenPET Control and Analysis Tools (OpenPET CAT) are data acquisition and analysis software for the OpenPET electronics based on the ROOT framework. OpenPET CAT utilizes the object-oriented design in providing basic utilities, control and analysis tools for the OpenPET electronics. Using OpenPET CAT requires installing the ROOT package (http://root.cern.ch). Using OpenPET CAT is optional; users can run the executables "openpet" (see Commands section) to configure the system and to acquire data directly.


Installing ROOT
===============

Go to http://root.cern.ch/drupal/content/downloading-root for instructions on downloading and installing ROOT. Although ROOT is supported on many platforms, OpenPET only supports the Windows version of ROOT using the Microsoft Visual C++ compiler. Current OpenPET CAT supports ROOT version 5.34/30 and Microsoft Visual C++ 2010. If you plan to develop and compile OpenPET CAT codes, you also need to install Microsoft Visual C++ 2010 Express, which can be downloaded at no cost from http://www.visualstudio.com/downloads/download-visual-studio-vs, and cygwin.


Using OpenPET CAT
=================

.. _8.2.1:

Configuration Files
-------------------

Defining an OpenPET system starts with a system configuration file as shown in :numref:`sys_config`.

The system configuration file defines:

* **Command Engine Type**: "0" for USB and "1" for Ethernet.
* **Command Engine Id**: "0"
* **Acquisition Engine Type**: "0" for USB and "1" for Ethernet.
* **Acquisition Engine Id**: "0"
* **Configuration Files Directory**: The directory path to the remaining configuration files.
* **MB Setup Files Prefix**: The file prefix to the Multiplexer Board configuration files.
* **MB Configuration**: The multiplexer board address (0 to 7).

Note: Any comments can be inserted between the "\*\*\*end ..." and the next "\*\*\*..." tags.

.. _sys_config:

.. figure:: ./_static/OpenPET_Users_Guide_Figures/Slide55.PNG
		:align: center
		:scale: 70%
		
		Example system configuration for a small system. 

Following the system configuration files, there are three more configuration files: 1) Multiplexer Board configuration file; 2) Detector Unit configuration file; and 3) Detector Board configuration file. For example, in :numref:`sys_config`, there is one multiplexer board with address 0 (note that in the small system, there are no physical MB board, so a dummy MB configuration file with address 0 is used). The location and prefix of this file in specified in "\*\*\*Configuration Files Directory\*\*\*" and "\*\*\*MB Setup Files Prefix\*\*\*" respectively. In this example, a file MB0.txt, as shown in :numref:`MBconfig`, would reside in the directory C:\Documents and Settings\seng\My Documents\OpenPET\software\small_system. MB0.txt shows that there is a Detector Unit with address 0, it is connected to MB0, and the configuration file for this Detector Unit is MB0_DU0.txt as shown in :numref:`DUconfig`. MB0_DU0.txt shows that the Detector Board type is the 16-Channel DB for each of the two DBs with addresses 0 and 2 in this Detector Unit. The configuration files for these Detector Boards are MB0_DU0_DB0.txt and MB0_DU0_DB2.txt. For example, MB0_DU0_DB0.txt is shown in :numref:`DBconfig`, which include all the DAC threshold and ADC gain settings for the 16-Channel DB. 

.. _MBconfig:

.. figure:: ./_static/OpenPET_Users_Guide_Figures/Slide83.PNG
		:align: center
		:width: 50%
		
		Example Multiplexer Board configuration file. 
		
		
.. _DUconfig:

.. figure:: ./_static/OpenPET_Users_Guide_Figures/Slide84.PNG
		:align: center
		:width: 50%
		
		Example Detector Unit configuration file. 



.. _DBconfig:

.. figure:: ./_static/OpenPET_Users_Guide_Figures/Slide57.PNG
		:align: center
		:scale: 70%
		
		Example Detector Board configuration file. 
		
		
.. _8.2.2:

OpenPET CAT Library and Macros
------------------------------

A library of C++ classes and ROOT macros are implemented to configure the system, acquire data and analyze the data. The OpenPET CAT software package can be downloaded from the OpenPET website (http://openpet.lbl.gov) under the "Downloads" menu. In the 'lib' directory, there is a library file called libopenpetcat.dll, which has to be copied to the directory <installed ROOT directory>/bin (i.e., $ROOTSYS/bin). In the 'examples' directory, there are macros and configuration files to help you get started. Three key macros are described below:

1.	acquireData.C(int acqTimeInSec, int initSystem=1)
This macro configures the system using configuration files and acquires a raw data file.
Inputs (arguments to the function):

  * acqTimeInSec - acquisition time in seconds
  * initSystem - configures the system (by default set to 1)
	
Inputs (edit in the macro file):

  * acqMode - see command ID 0x0003
  * dataFormat - currently not implemented yet
  * numADCsample - 0 to 512; see command ID 0x0005
  * numPreTrigSample - 0 to 16; see command ID 0x0005
  * numTrigWindowSample - 0 to 16; see command ID 0x0005
  * configFile - system configuration file
  * filename - raw data filename (the date will be prepended to this name)

Example 1: root> .x acquireData.C(100)

	This macro initializes the system using the configuration files and acquires data for 100 s.

Example 2: root> .x acquireData.C(100, 0) 

	This macro acquires data for 100 s without initializing the system.

2.	displayWaveform.C
This macro displays the acquired waveforms event-by-event.
Inputs (edit in the macro file):

  * numSkippedEvent - number of events to be skipped
  * evtTimeWindow - not supported yet
  * dataFilename - filename of the raw data file

Example: root> .x displayWaveform.C

3.	analyzeRawData.C
This macro reads the waveforms event-by-event and integrates each waveform to calculate its energy. It also outputs a root file containing an ntuple with the energy and tdc values for every channel. This macro can only be used when only one 16-channel Detector Board is in the system.
Inputs (edit in the macro file):

  * numSkippedEvent - number of events to be skipped
  * evtTimeWindow - not supported yet
  * startBaselineBin - start bin for calculating the baseline of the waveform
  * endBaselineBin - end bin for calculating the baseline of the waveform
  * startIntegratingBin - start bin for calculating the energy of the waveform
  * endIntegratingBin - end bin for calculating the energy of the waveform
  * dataFilename - filename of the raw data file
  * rootFilename - output ROOT filename

Example: root> .x analyzeRawData.C



OpenPET CAT Graphic User Interface
==================================

Introduction
------------

The OpenPET CAT Graphic User Interface (GUI) is a user interface for the OpenPET CAT data acquisition framework. It was developed so that users may conveniently use the OpenPET CAT software to gather and analyze data from the OpenPET system. There are two user interfaces: the OpenPET Control and Analysis - Data Acquisition (openpet_cat_acq.exe) and the OpenPET Control and Analysis - Root Analyzer (openpet_cat_ana.exe). The first one is used for configuring the system, gathering data, and generating the ROOT file. The second one is used for analyzing the ROOT file. 

.. _8.3.2: 

OpenPET Control and Analysis - Data Acquisition
-----------------------------------------------

Once the data acquisition application is launched, there are two screens that will appear. One will be the user interface titled "OpenPET Control and Analysis Tools - Data Acquisition," and the other is a status window entitled "ROOT session." :numref:`dataGUI_initial` shows this initial display.

The Data Acquisition GUI has four functionalities associated with it. First, it can initialize the system based on the system configuration files discussed in `Configuration Files`_ It also allows the user to acquire data in the Acquire Data tab and display the waveform events in the Display Waveform tab. In addition, the data file may be converted into a ROOT file in the Generate ROOT File tab. These four functionalities are discussed in detail below.

.. _dataGUI_initial:

.. figure:: ./_static/OpenPET_Users_Guide_Figures/Slide58.PNG
		:align: center
		
		Data Acquisition GUI Initial Display

Configure System
^^^^^^^^^^^^^^^^

The initial screen of the user interface is the system configuration window (:numref:`sys_config_tab`). This tab will display all hardware configuration data loaded from the system configuration file discussed in `Configuration Files`_. The top section displays the engine settings and configuration file directory. The bottom section displays the prefixes and address lists of Multiplexer Boards, Detector Units, and Detector Boards along with an editable text window for the Detector Board configuration file. There is also the Reconfigure System button and Initialize System button. The Reconfigure System button reconfigures the system should any values in the Detector Board configuration file be changed by the user. The Initialize System button initializes the system. Both buttons are disabled until a system configuration file is loaded.

.. _sys_config_tab:

.. figure:: ./_static/OpenPET_Users_Guide_Figures/Slide59.PNG
		:align: center
		
		Sections of the Configuration tab
		

To load a system configuration file, click on the File menu in the top left hand corner and select Read System Configuration File. This will open a dialog window allowing the user to select the desired system configuration file as shown in :numref:`config_file_open`. Once the file is selected, the system will configure. The user may check the status of the configuration process by reading the ROOT session window. An example of a completed system configuration is shown in :numref:`config_session`.

.. _config_file_open:

.. figure:: ./_static/OpenPET_Users_Guide_Figures/Slide60.PNG
		:align: center
		
		Opening system configuration file
		
.. _config_session:

.. figure:: ./_static/OpenPET_Users_Guide_Figures/Slide61.PNG
		:align: center
		
		ROOT Session Example
		


After the system has been configured, the top section will be filled with the appropriate values along with the configuration file path. Also, the configuration file of the first Detector Board of the first Multiplexer Board will appear. The MB, DU, and DB addresses shown specify the address of the configuration file being displayed. To change, select the desired MB, DU, and DB address combination from the dropdown lists to show that particular Detector Board's configuration file.

If the user is satisfied with the current configuration at this point, the user may initialize the system by clicking the Initialize System button. If not, the user can change values in a particular Detector Board's configuration file by selecting the correct MB address, DU address, and DB address to open the desired configuration file as shown in :numref:`DBconfig_file`. As the user makes changes, the configuration file is saved automatically.  However, to have these changes take effect in the system, the system must be reconfigured by clicking the Reconfigure System button. Then the system may be initialized.  

.. _DBconfig_file:

.. figure:: ./_static/OpenPET_Users_Guide_Figures/Slide62.PNG
		:align: center
		
		Detector Board configuration file

Again, the user can observe the status of the system by reading the terminal window. The command and response IDs discussed in :ref:`commands` will be printed there. Therefore, the user can determine if the system was correctly initialized.  

Acquire Data
^^^^^^^^^^^^

Once the system has been initialized, the Acquire Data button will now activate so the user can acquire data.  There are four parameters the user must set which are the following (shown in :numref:`acq_data_tab`):

  * Data Mode - see command ID 0x2201
  * Data Format - see command ID 0x2203 for configuring oscilloscope mode
  * Number of ADC samples - 1 to 241 (see command ID 0x2203)
  * Acquisition time - acquisition time in seconds

.. _acq_data_tab:

.. figure:: ./_static/OpenPET_Users_Guide_Figures/Slide63.PNG
		:align: center
		
		Acquire data window


The previous values that were used will be set as default values.  

The data file will be saved under the current working directory, which is displayed at the bottom of the window. The filename itself will have the format yyyymmdd_hhmmss_openpet.dat where the current date and time are used as the prefixes.  

  Example: 20140717_090251_openpet.dat -- Data file stored on July 17, 2014 at 9:02:51AM

Once the parameters are set, the user may click the Acquire Data button to begin retrieving data.  


Display Waveform
^^^^^^^^^^^^^^^^

:numref:`disp_wave_tab` shows the Display Waveform tab. The functionality of this window is the same as the displayWaveform macro in `OpenPET CAT Library and Macros`_. The user may load the desired data file to display the acquired waveforms event by event. The window is arranged vertically in four main sections. The top section is the user input settings. These settings are required to analyze the data file and are set to be the same values as in the Acquire Data tab. The next section is the data file section, which includes the button to load a data file and a field that will display the name of the current data file open. Next is the event section where the address and event number of the waveform currently displayed are shown, along with the button to cycle to the next event. The fourth section is the canvas where the waveforms will be displayed.

.. _disp_wave_tab:

.. figure:: ./_static/OpenPET_Users_Guide_Figures/Slide64.PNG
		:align: center
		
		Initial Display Waveform tab

To activate the Load Data File button, the user must first configure the system with the same configuration used to gather the data file. If a different system configuration file has already been loaded, the user can read in the correct system configuration file to match the configuration used with that data file. In either case, the steps to load the system configuration file are equivalent to the instructions in `OpenPET Control and Analysis - Data Acquisition`_ and can be executed while in the Display Waveform tab. The user can load the data file by clicking on the Load Data File button. A file dialogue will appear. Select the desired file and open it. Once opened, the first event waveforms will appear in the canvas, and the address and event number will appear above it. The Next Event button will also activate at this time.

.. _disp_wave:

.. figure:: ./_static/OpenPET_Users_Guide_Figures/Slide65.PNG
		:align: center
		
		Display Waveform

The waveforms are displayed by channel from left to right, starting in the top left corner. As shown in :numref:`disp_wave`, the top left waveform is channel 0, the next one to the right is channel 1, and so on with the last channel on the bottom right corner. The address (MB, DU, DB) of the event currently displayed is shown above the canvas on the left along with the current event number. To the right of the address and event number is the Next Event button. Clicking on the Next Event button will display the next event waveforms. 

In :numref:`disp_wave`, the number of skipped events is set to the default zero. However, if the user does not want to start at the beginning of the file, the user may skip to a specific event number. For example, if the user wants to skip 10 events, the user would input ten as the desired number of skipped events and load the data file again. The opening waveforms would then display event ten as shown in :numref:`event_skip`.

.. _event_skip:

.. figure:: ./_static/OpenPET_Users_Guide_Figures/Slide66.PNG
		:align: center
		
		Example highlighting number of skipped events
		

Generate ROOT File
^^^^^^^^^^^^^^^^^^

The fourth tab allows the user to generate a ROOT file from the data file. This window is divided into three sections as shown in :numref:`genROOTtab`. The first section is the user input settings that specifies the data parameters. The second section is the data file section. It contains a text box that will display the file path of the loaded data file and the Load Data File button so the user can upload the data file. The final section contains the ROOT file parameters which are bin parameters required for calculating energy histograms that need to be set by the user. The previous values used are set as the default values. 

To generate the ROOT file, the same system configuration file used in gathering the data must be currently loaded before loading the data file. Also, the user input settings must be the same as those used for gathering data. The default values are the same current values in the Acquire Data tab. Now, the user may click on the Load Data File button and a file dialogue will appear, allowing the user to select the desired file. Once the file is selected and the user is satisfied with the ROOT file parameters, he may click on the Generate ROOT File button. The button will not activate until a data file is loaded. Another file dialogue will appear, and the user can navigate to the directory where he would like to save the ROOT file. Then he must type in the filename **ending with the file extension "root".** Click save and the ROOT file will begin generating. The sequence of steps is outlined in :numref:`saveROOTfile`.

.. _genROOTtab:

.. figure:: ./_static/OpenPET_Users_Guide_Figures/Slide67.PNG
		:align: center
		
		Generate ROOT File tab
		
		
.. _saveROOTfile:

.. figure:: ./_static/OpenPET_Users_Guide_Figures/Slide68.PNG
		:align: center
		
		Sequence of steps for saving a ROOT file
		


The status of the ROOT file may be checked by looking at the ROOT Session window. The window will print out every one thousand events read and the total bytes read. The ROOT file is completed when the statement "ROOT file written" is printed. An example is illustrated in :numref:`ROOTsession`.

.. _ROOTsession:

.. figure:: ./_static/OpenPET_Users_Guide_Figures/Slide69.PNG
		:align: center
		
		Generating ROOT File Session


OpenPET Control and Analysis - ROOT Analyzer
--------------------------------------------

Once the ROOT Analyzer application is launched, two screens will appear. One will be the user interface titled "OpenPET Control and Analysis Tools - ROOT Analyzer," and the other will be a status window entitled "ROOT session." :numref:`analyzerGUI_initial` shows the initial startup of the ROOT Analyzer.

.. _analyzerGUI_initial:

.. figure:: ./_static/OpenPET_Users_Guide_Figures/Slide70.PNG
		:align: center
		
		ROOT Analyzer GUI Initial Display
		

The ROOT Analyzer GUI has two main functionalities. The first is displaying a variety of histograms such as detector board hitmap histograms and energy, baseline, and TDC histograms for specific channels in the Display Histograms tab. Secondly, for block sensors, the floodmap may be displayed in the Display Floodmap tab. These two functionalities are discussed in detail below.

.. _analyzer_layout:

.. figure:: ./_static/OpenPET_Users_Guide_Figures/Slide71.PNG
		:align: center
		
		ROOT Analyzer window layout
		

The window is laid out in three tab panels (:numref:`analyzer_layout`).  On the left is the Files tab which allows the user to open a ROOT file without opening a file dialogue.  The middle panel is the Parameters Tab which has two tabs namely Histograms and Floodmap.  On the right is the Canvas Tab which also has two tabs called Display Histograms and Display Floodmap.  The middle Histograms tab contains parameters to display histograms on the Display Histograms canvas and the middle Floodmap tab contains parameters to display the floodmap on the Display Floodmap canvas.  Each middle tab is linked to display its associated canvas and vice versa.  For example, the user cannot select the middle Floodmap tab and be showing the Display Histograms canvas.

Before any analysis can be displayed, a ROOT file must be loaded first (:numref:`openingROOTfile`).  There are two different ways a user can open a ROOT file.  The first method is by using the Files tab on the left side of the window.  The user can navigate to the desired ROOT file by double clicking on folder icons to reach the desired directory.  The folder titled ".." moves up one directory.  The Files tab only displays directories or ROOT files.  The second method to open a ROOT file is by using the File menu.  To do so, the user must click on the File menu at the top left and select Read ROOT File. This will open a file dialogue displaying only ROOT files. The user can then select the desired ROOT file to open.

.. _openingROOTfile:

.. figure:: ./_static/OpenPET_Users_Guide_Figures/Slide72.PNG
		:align: center
		
		Opening ROOT File - on the left is the first method mentioned and on the right is the second method.
		

Display Histograms
^^^^^^^^^^^^^^^^^^

Once the ROOT file is opened, the hit histogram of the first Detector Board from the first Detector Unit of the first Multiplexor Board will be displayed as shown in :numref:`DBhit`. To see a different detector board, click the black arrows on the right side of the MB, DU, and DB address boxes to open a drop down list so that the desired MB, DU, and DB addresses may be selected. The first DB histograms for any given MB and DU combination is always displayed first until a different one is selected by the user. To display a different histogram, use the dropdown Histogram list to select the desired option as illustrated in :numref:`DBhit`.

.. _DBhit:

.. figure:: ./_static/OpenPET_Users_Guide_Figures/Slide73.PNG
		:align: center
		
		Displaying First DB Hit
		

When viewing the energy, baseline, or TDC histogram, the user can change which channel to display by clicking on the black arrow next the channel number box.  This will open a drop down menu from which the desired channel can be selected.  The user can also adjust the binning of the histogram by clicking the Rebin button after changing the starting bin value, ending bin value, and the number of bins.  An example is shown below in :numref:`DBenergy`.

.. _DBenergy:

.. figure:: ./_static/OpenPET_Users_Guide_Figures/Slide74.PNG
		:align: center
		
		Displaying DB Energy Histogram
		

Display Floodmap
^^^^^^^^^^^^^^^^

For block detectors, the second pair of parameter and canvas tabs generates a floodmap for a user-defined combination of channels. The parameter Floodmap tab will be described top to bottom based on :numref:`floodmap`. At the top is the Address Section.  This section contains the address of the current detector board along with the channels selected to generate the floodmap. There are four channel drop-down lists, one for each channel of the block detector. The channel names A through D correspond to the A through D values in the Equation Section.  The Address Section also contains the Display Floodmap button.  Next is the Bin Section which allows the user to rebin the histogram.  The last section at the bottom is the Equation Section which shows the equations used to generate the floodmap. 

To display a floodmap, the user must select the correct detector board if not already selected. After that, the user must determine which channels are the correct channels corresponding to variables A through D in the equations. Once the channels are selected, click on the Display Floodmap button. An example of a floodmap display is shown in :numref:`floodmap_example`. The floodmap can also be rebinned. The user can input the numbers for rebinning the X or Y values in the Bin Section and click the Rebin button.

.. _floodmap:

.. figure:: ./_static/OpenPET_Users_Guide_Figures/Slide75.PNG
		:align: center
		
		Initial Floodmap Window
		
		
.. _floodmap_example:

.. figure:: ./_static/OpenPET_Users_Guide_Figures/Slide76.PNG
		:align: center
		
		Floodmap Example
		
		
		
