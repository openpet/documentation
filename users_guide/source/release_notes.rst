.. This page is the Release Notes section

.. role:: red

.. role:: crop

.. role:: bash(code)
   :language: bash

.. index:: Release Notes

.. _release-notes:

***************************
OpenPET v2.3.2 Release Notes
***************************


What's New
===========
Firmware
--------
* Bug fixes.

SupportBoard
^^^^^^^^^^^^
* Bug fixes.

DetectorBoard
^^^^^^^^^^^^^
* Bug fixes.


Embedded Software
-----------------
* No major changes.

SupportBoard
^^^^^^^^^^^^
Main FPGA
"""""""""
* No changes.


IO FPGAs
""""""""
* No changes.

DetectorBoard
^^^^^^^^^^^^^
* No changes.

Software
--------
* Bug fixes.




Hardware
--------
* No changes.


Upgrading from Previous Releases of OpenPET
============================================
* Standard system changes are not backward compatible.

