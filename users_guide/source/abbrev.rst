.. This page is for Abbreviations

Abbreviations
=============


| CDUC: Coincidence Detector Unit Controller
| CI: Coincidence Interface Board
| C/R: Commands and Responses
| CU: Coincidence Unit
| CUC: Coincidence Unit Controller
| DB: Detector Board
| DST: Destination
| DU: Detector Unit
| DUC: Detector Unit Controller
| EPCS: Enhanced Programming Configuration Serial device
| FIFO: First-in, First-out Data Buffer
| FPGA: Field-Programmable Gate Array
| MB: Multiplexer Board
| MBC: Multiplexer Board Controller
| PLL: Phase-Locked Loop
| SB: Support Board
| SRC: Source
| TDC: Time-to-Digital Converter

